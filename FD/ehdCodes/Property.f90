
subroutine Property

use NEnumber

implicit none

Real :: Length,FL

! THE PARAMETERS WHICH ARE ASIGNED BY "@" SHOULD BE MODIFIED WHEN GEOMETRY OR CHARACTERS OF FLUID ARE CHANGED
! .... General Properties ...........................................

   mu     = 1.80e-5   ! Viscosity	           
   ru     = 1.2       ! Density
   dt     = 1.0e-2    ! Time Step	
   Length = abs(x(1) - x(N))  ! Domain Length	
   FL     = 0.2        ! Free Stream Part Of Domain
   Lc     = Length - FL! Specific Length of Flat Plate			
   Ui     = 1.25  !3.7 ! Inlet Velocity
   Re     = ui * ru * Lc / mu! Reynolds Number			

! .... Turbulence Properties ........................................

   Rmu    = 2.0 !6.0                     ! Viscosity Ratio				@
   Tui    = 5.63147 !8.646               ! Inlet Turbulence Intensity
   Kti    = 1.5 * (Tui / 100.0 * Ui)**2.0! Inlet Kinetic Energy 
   Omegai = ru *Kti / Rmu / mu  ! Inlet Dissipation Per Turbulent Kinetic Energy
   Omginf = Omegai  ! Far-Field Dissipation Per Turbulent Kinetic Energy
   Ktinf  = Kti     ! Far-Field Kinetic Energy

! .... Transition Properties ........................................

   intmi = 1.0
   TuL = ((Tui / 100.0)**2.0 * (1.0 + (3.0 * ru * Ui * FL * 0.09 *(Tui / 100.0)**2.0) &
         / (2.0 * mu * Rmu))**(-0.0828 / 0.09))**0.5

   if (Tui <= 1.3) then
      Remti = 1173.51 - 589.428 * Tui + 0.2196 / Tui**2.0
   else if (Tui > 1.3) then
      Remti = 331.50 * (Tui - 0.5658)**(-0.671)
   end if

! ..................................................................................
   Print*,' ....  Inelt Value ..................................................'
   Print*,''
   Print*,'  Inlet Velocity=',Ui,'m/s'
   Print*,'  Reynolds Number=',Re
   Print*,'  Mach Number=',Ui/340.0
   Print*,'  B.L. Thickness In Outlet (Laminar FP)=',0.37*Lc/(Re**0.2),'m'
   Print*,'  Inlet Turbulence Intensity=', Tui, '%'
   Print*,'  Turbulence Intensity at Leading Edge =',TuL*100.0, '%'
   Print*,'  Viscosity Ratio (mut/mu)=  ', Rmu
   Print*,'  Inlet Kinetic Energy = ', Kti
   Print*,'  Inlet Dissipation Per Turbulent Kinetic Energy= ' ,Omegai
   Print*,'  Inlet Intermittency= ' ,intmi
   Print*,'  Inlet Transition Onset Momentum Thickness Re= ', REmti
   Print*,''
   Print*,'......................................................................'
   pause

   Return

End Subroutine Property
