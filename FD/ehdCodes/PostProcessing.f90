

Subroutine PostProcessing

Use NEnumber


! ##############################################################################
! IN THE SUBROUTINE, THE POST PROCESSING STEP OF MODELING IS DONE, 
! Y+, U+, SHEAR STRESS ON THE WALL, FRICTION COEFFICIENT, PRESSURE COEFFICIENT 
! BY USING VALUES OF NUMERICAL MODELING ARE CALCULATED
!	
! WSS : SHEAR STRESS ON THE WALL
! Y_P : Y+
! U_P : U+
! NUS : FRICTION VELOCITY
! CF : FRICTION COEFFICIENT ON THE SURFACE (N --> NUMERICAL; T --> THEORITICAL)
! CP : PRESSURE COEFFICIENT ON THE SURFACE
!
! ############################################################################## 

Implicit None

Integer         :: ki

Real (Kind = 8) :: WSS, WSSm1, Rex, Cfn, Cft, Cfl, U_P, Y_P, nus, Cp, Pw


   19 Format (10E24.5)
   17 Format (7E24.5)

   ki = 0
   Write(60,*)'VARIABLES = "Yp","LogY+", "Rmu", "KE", "Omega", "F1" , "F2", "U+a", "U+n","Tur_RS"'
   Write(70,*)'VARIABLES = "Rex","Cfn", "Cft", "Cfl", "WSS", "Cp", "X"'


   do i = sp , E , n1
      ki = ki + 1
      WSS = 2.0 * (mu + Mut(i)) * Strt12(Sp)
      nus = sqrt(abs(WSS / ru))
      Y_P = wd(i) * nus * ru / mu
      if (Y_P < 5.0) U_P = Y_P
      if (Y_P > 30.0 .and. Y_P < 500) U_P = log(Y_P) / 0.41 + 5.0

      Write(60,19) Yp(i),log10(Y_P), Mut(i) / mu, Kt(i), Omega(i), Ft1(i), Ft2(i), &
                    U_p,U(i) / nus,2.0 * Mut(i) * Strt12(i) / Ui**2.0
   end do

   do i = 1 , E
      do t = 1 , 4
         if (FP(i,t) == 1) then
            WSSm1 = WSS
            Rex = (ui * ru * Xp(i) / mu)
            Cft = 0.025 * Rex**(-1.0 / 7.0)
            Cfl = 0.664 / sqrt(abs(Rex))

            WSS = (mu + Mut(i)) * Strt12(i)
            if ((WSSm1 > 0.0 .and. WSS < 0.0) .or. (WSSm1 < 0.0 .and. WSS > 0.0)) & 
                POS = i
            Cfn = WSS * 2.0 / ru / Ui**2.0

            Pw = 0.5 * (P(i) - P(bnc(i,t)))
            Cp = Pw / ru / Ui**2.0 * 2.0
            write(70,17) Rex, Cfn, Cft, Cfl, WSS, Cp, Xp(i)
        end if
     end do
   end do
   
   close(60)
   close(70)

   Return

End Subroutine PostProcessing
