
Subroutine Tu_KO_Gradient

Use NEnumber

! ##############################################################################
! IN THE SUBROUTINE, GRADIENT OF TURBULENT EQUATION PARAMETERS (KE,OMEGA) IN ALL 
! CELLS ARE CALULATED. FOR THIS, GAUSS'S THEOREM IS USED.
!	
! Kx , Omgx : GRADIENT IN X DIRECTION
! Ky , Omgy : GRADIENT IN Y DIRECTION
!
! ############################################################################## 	

Implicit none

Integer       :: m1,m2

Real (Kind=8) :: dx,dy

	Kx=0.0
	Ky=0.0
	Omgx=0.0
	Omgy=0.0
	do i=1,E
	  do j=1,4
         select case (j)
	       case(1)
	        m1=1
		    m2=2
           case(2)
   	        m1=2
		    m2=3
           case(3)
            m1=3
		    m2=4
		   case(4)
            m1=4
		    m2=1
         end select
         dx=x(Con(i,m2))-x(Con(i,m1))
	     dy=y(Con(i,m2))-y(Con(i,m1))
		 if(Neibr(i,j)/=0) then   
			Kx(i)=Kx(i)+dy*((1.0-f(i,j))*Kt(i)+f(i,j)*Kt(Neibr(i,j)))/ds(i)
		    Ky(i)=Ky(i)-dx*((1.0-f(i,j))*Kt(i)+f(i,j)*Kt(Neibr(i,j)))/ds(i)
			Omgx(i)=Omgx(i)+dy*((1.0-f(i,j))*Omega(i)+f(i,j)*Omega(Neibr(i,j)))/ds(i)
		    Omgy(i)=Omgy(i)-dx*((1.0-f(i,j))*Omega(i)+f(i,j)*Omega(Neibr(i,j)))/ds(i)
		 else
			Kx(i)=Kx(i)+dy*Ktf(i,j)/ds(i)
		    Ky(i)=Ky(i)-dx*Ktf(i,j)/ds(i)
			Omgx(i)=Omgx(i)+dy*Omgf(i,j)/ds(i)
		    Omgy(i)=Omgy(i)-dx*Omgf(i,j)/ds(i)
         end if			  
      end do	  	
	end do

Return

End Subroutine Tu_KO_Gradient