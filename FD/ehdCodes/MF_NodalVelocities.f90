

Subroutine MF_NodalVelocities
   
Use NEnumber

! ##############################################################################
! IN THE SUBROUTINE, NODAL VALUE OF PARAMETERS OF NS EQUATION ARE CALCULATED 
! REGARDING BOUNDARY CONDITIONS
!	
! Un & Vn & Pn : NODAL VALUES OF VELOCITY AND PRESSURE
!
! ############################################################################## 
Implicit None

Integer  :: m1,m2 


   Un = 0.0
   Vn = 0.0
   Pn = 0.0
   do i = 1 , N
      do j = 1 , CV(i,1)
         Un(i) = Un(i) + u(CV(i,j+1)) * f2(i,j)
         Vn(i) = Vn(i) + v(CV(i,j+1)) * f2(i,j)
         Pn(i) = Pn(i) + p(CV(i,j+1)) * f2(i,j)
      end do
   end do

   do i = 1 , E
      do t = 1 , 4
         select case (t)
            case(1)
               m1 = 1
               m2 = 2
            case(2)
               m1 = 2
               m2 = 3
            case(3)
               m1 = 3
               m2 = 4
            case(4)
               m1 = 4
               m2 = 1
         end select
         if (FP(i,t) == 4) then
            Un(Con(i,m1)) = Ui
            Un(Con(i,m2)) = Ui
            Vn(Con(i,m1)) = 0.0
            Vn(Con(i,m2)) = 0.0

         else if (FP(i,t) == 7) then    ! Symmetric Surface Upstream of the Flat Plate
            if (CV(Con(i,m1),1) /= 1 .and. CV(Con(i,m2),1) /= 1) then
               Un(Con(i,m1)) = (Uf(CV(Con(i,m1),2),t) + Uf(CV(Con(i,m1),3),t)) / 2.0
               Un(Con(i,m2)) = (Uf(CV(Con(i,m1),2),t) + Uf(CV(Con(i,m1),3),t)) / 2.0
               Vn(Con(i,m1))=0.0     !(Vf(CV(Con(i,m1),2),t)+Vf(CV(Con(i,m1),3),t))/2.0
               Vn(Con(i,m2))=0.0     !(Vf(CV(Con(i,m1),2),t)+Vf(CV(Con(i,m1),3),t))/2.0
            end if

         else if(FP(i,t) == 1) then
             Un(Con(i,m1)) = 0.0
             Un(Con(i,m2)) = 0.0
             Vn(Con(i,m1)) = 0.0
             Vn(Con(i,m2)) = 0.0

         else if(FP(i,t) == 3) then
             if (CV(Con(i,m1),1) /= 1 .and. CV(Con(i,m2),1) /= 1) then
                Un(Con(i,m1)) = (Uf(CV(Con(i,m1),2),t) + Uf(CV(Con(i,m1),3),t)) / 2.0
                Un(Con(i,m2)) = (Uf(CV(Con(i,m1),2),t) + Uf(CV(Con(i,m1),3),t)) / 2.0
                Vn(Con(i,m1)) = (Vf(CV(Con(i,m1),2),t) + Vf(CV(Con(i,m1),3),t)) / 2.0
                Vn(Con(i,m2)) = (Vf(CV(Con(i,m1),2),t) + Vf(CV(Con(i,m1),3),t)) / 2.0
             else if  (CV(Con(i,m1),1) == 1) then
                Un(Con(i,m1)) = Uf(CV(Con(i,m1),2),t)
                Vn(Con(i,m1)) = Vf(CV(Con(i,m1),2),t)
             else if  (CV(Con(i,m2),1) == 1) then
                Un(Con(i,m2)) = Uf(CV(Con(i,m1),2),t)
                Vn(Con(i,m2)) = Vf(CV(Con(i,m1),2),t)
             end if

         else if (FP(i,t) == 2) then
             if (CV(Con(i,m1),1) /= 1 .and. CV(Con(i,m2),1) /= 1) then
                Un(Con(i,m1)) = (Uf(CV(Con(i,m1),2),t) + Uf(CV(Con(i,m1),3),t)) / 2.0
                Un(Con(i,m2)) = (Uf(CV(Con(i,m2),2),t) + Uf(CV(Con(i,m2),3),t)) / 2.0
                Vn(Con(i,m1)) = (Vf(CV(Con(i,m1),2),t) + Vf(CV(Con(i,m1),3),t)) / 2.0
                Vn(Con(i,m2)) = (Vf(CV(Con(i,m2),2),t) + Vf(CV(Con(i,m2),3),t)) / 2.0
             end if

         else if (FP(i,t) == 6) then    ! Surface of Airfoil
             Un(Con(i,m1)) = 0.0
             Un(Con(i,m2)) = 0.0
             Vn(Con(i,m1)) = 0.0
             Vn(Con(i,m2)) = 0.0
         end if 
      end do
   end do

   Return

End Subroutine MF_NodalVelocities
