

Subroutine Tr_Intermittency

Use NEnumber

Implicit None

Real (kind = 8) :: IntmNC,Intmnew,Fr

  
  ErrorI = 0.0
  Fr     = 0.0

  do i = 1 , E
      intmnew = (Int_Exp(i) + Prod_int(i) - Sc_int(i)) / apin(i)
      do t = 1 , 4
         if (Neibr(i,t) /= 0) then
            IntmNC = intm(Neibr(i,t))
         else 
            IntmNC = intmf(i,t)
         end if
         
         intmnew = intmnew + anin(i,t) * IntmNC / apin(i)
      end do

      ErrorI  = ErrorI + abs(Intmnew - Intm(i))
      Fr      = Fr + abs(Intm(i))
      Intm(i) = Intmnew * AlphaI + Intm(i) * (1.0 - AlphaI)
  end do
  
  ErrorI = ErrorI / Fr

  Return

End Subroutine Tr_Intermittency




