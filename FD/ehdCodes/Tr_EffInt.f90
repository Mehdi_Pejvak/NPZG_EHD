

Subroutine Tr_EffInt

Use NEnumber

Implicit None


Real (kind = 8) :: Sep_int,SIP,Freattach

    do i = 1 , E
       Freattach = exp(-(Rt(i) / 20.0)**4.0)
       SIP       = max(0.0,(Rev(i) / 3.235 / Remc(i) - 1.0))
       Sep_int   = min(s1 * SIP * Freattach,2.0) * Fmt(i)
       Eff_int(i)= max(intm(i),Sep_int)
    end do

    Return

End Subroutine Tr_EffInt
