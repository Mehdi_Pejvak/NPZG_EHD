

Subroutine Turb_Coef

Use NEnumber

! ##############################################################################
! IN THE SUBROUTINE, COEFFICIENTS OF DISCRITISED TURBULENCE MODEL'S EQUATIONS BASED ON FV METHOD 
! ARE CALCULATED. DUE TO USING GUASS'S THEOREM IN CALCULATING INTEGRAL, 
! TURBULENT VISCOSITY ON FACE NEEDS TO BE CALCULATED.
! DUE TO SATISFICTION OF MASS FLUX IN CELL, MASS FLUX IS REMOVED FROM 
! CALCULATION OF AP.
! ##############################################################################


Implicit None

Real (Kind=8) :: sgmke,sgmoe,Mue


   do i=1,E
      apk(i)=-Spk(i)  !+ru*ds(i)/dt	
	  apom(i)=-Spomg(i) !+ru*ds(i)/dt	
      do j=1,4
		if (Neibr(i,j)==0) then
			if (FP(i,j)==1) then	! Solid Boundary (Mut=0)
				Mue=0.0
			else
				Mue=Mut(i)
				sgmke=sgmk(i)
				sgmoe=sgmo(i)
			end if
		else
			Mue=(1.0-f(i,j))*Mut(i)+f(i,j)*Mut(Neibr(i,j))
			sgmke=(1.0-f(i,j))*sgmk(i)+f(i,j)*sgmk(Neibr(i,j))
			sgmoe=(1.0-f(i,j))*sgmo(i)+f(i,j)*sgmo(Neibr(i,j))
		end if	
		ank(i,j)=max(Z,-massf(i,j))+(mu+sgmke*Mue)*L(i,j)/M(i,j)
		apk(i)=apk(i)+ank(i,j)	!+massf(i,j)
		anom(i,j)=max(Z,-massf(i,j))+(mu+sgmoe*Mue)*L(i,j)/M(i,j)
		apom(i)=apom(i)+anom(i,j)	!+massf(i,j)
	  end do
   end do

Return

End Subroutine Turb_Coef