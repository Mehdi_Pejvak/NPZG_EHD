
Subroutine Tu_KE

Use NEnumber		

! ##############################################################################
! IN THE SUBROUTINE, KINETIC ENERGY (K) IS CALCULATED BY USING ALL SOURCE TERMS AND EXPLICIT 
! TERMS CALCULATED IN OTHER SUBROUTINE, AND THEN VALUE OF ERROR OF KE IS CALCULATED
!	
! ERROR=SIGMA (Ktnew-Kt)/SIGMA(AP*Kt)	
! Ktnew: VALUE OF CURRENT STEP	
! Kt : VALUE OF PREVIOUS STEP AND IN FINAL STEP, VALUE OF CURRENT ONE BY APPLYING UNDERRELAXATION FACTOR
! SIGMA : SUM OF ALL CELLS' VALUE
!
! ############################################################################## 

Implicit None

Real (Kind=8) :: KtNC,Ktnew,Fr,Dk


  Errork=0.0
  Fr=0.0
  do i=1,E	
	Ktnew=(KE_Exp(i)+Sck(i)+Phk(i))/apk(i)
	do t=1,4
		if (Neibr(i,t)/=0) then
			KtNC=Kt(Neibr(i,t))
		else 
			KtNC=Ktf(i,t)
		end if
		Ktnew=Ktnew+ank(i,t)*KtNC/apk(i)
	end do
	Errork=Errork+abs(Ktnew-Kt(i))
	Fr=Fr+abs(Kt(i))
	Kt(i)=Ktnew*Alphak+Kt(i)*(1.0-Alphak)
  end do
  Errork=Errork/Fr

Return

End Subroutine Tu_KE