

Subroutine MF_BC 

Use NEnumber

Implicit None

! ##############################################################################
! IN THE SUBROUTINE, BOUNDARY CONDITIONS FOR NS EQUATIONS (VELOCITIES) ARE 
! CALCULATED ACCORDING TO ACHIEVED DATA IN CORRESPONDING EQUATIONS.
! FOR OUTLET, IN ORDER TO SATISFACTION OF CONSERVATION OF MASS FLUX, VELOCITIES 
! ARE MULTIPLIED IN MASS RATION (INLET MASS FLUX / OUTLET MASS FLUX)
! IN NON-ZERO PRESSURE GRADIENT, A VARIABLE CROSS-SECTIONAL AREA ALONG THE FLOW 
! DIRECTION IS APPLIED. THE FORMULA OF THE PROFILE OF THE VARIABLE CROSS SECTION 
! ARE USED FROM "Correlations for modeling transitional boundary layers under 
! influences of freestream turbulence and pressure gradient" 
! ##############################################################################


    do i = 1,E
       do t = 1,4
          if (FP(i,t) == 1) then
             Uf(i,t) = 0.0
             Vf(i,t) = 0.0
          else if (FP(i,t) == 2) then
             Uf(i,t) = 4.0 / 3.0 * U(i) - 1.0 / 3.0 * U(BNC(i,t))
             Vf(i,t) = 4.0 / 3.0 * V(i) - 1.0 / 3.0 * V(BNC(i,t))
             Uf(i,t) = Uf(i,t) * m_ratio
             Vf(i,t) = Vf(i,t) * m_ratio
          else if (FP(i,t) == 3) then
             Uf(i,t) = 0.3 * Ui / (Y(Con(i,3)) + Y(Con(i,4))) * 2.0
             Vf(i,t) = (Y(Con(i,3)) - Y(Con(i,4))) / (X(Con(i,3)) - X(Con(i,4))) * Uf(i,t)
          else if (FP(i,t) == 4) then
             Uf(i,t) = Ui
             Vf(i,t) = 0.0
          else if (FP(i,t) == 6) then    ! Surface of Airfoil
             Uf(i,t) = 0.0
             Vf(i,t) = 0.0
          else if (FP(i,t) == 7) then    ! Symmetric Surface Upstream of the Flat Plate
             Uf(i,t) = U(i)
             Vf(i,t) = 0.0    !V(i)
          end if
       end do
    end do

Return

End Subroutine MF_BC 


