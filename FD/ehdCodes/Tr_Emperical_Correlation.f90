

Subroutine Tr_Emperical_Correlation

Use NEnumber

Implicit None

Integer         :: ni

Real (kind = 8) :: Flmt,lanmt(E),mt(E),mts,KEC(E), &
                   Tu(E),Utx(E),Uty(E),Uts(E),Kac(E),Fac, &
                   errormax,lanmtnew(E),Fk,Flanda
                   
Character(30)   :: EmFor

   EmFor = "sulu"
   
   do i = 1 , E
      Ut(i)  = sqrt(U(i) * U(i) +V(i) * V(i))
      Tu(i)  = 100.0 * sqrt(2.0 * abs(Kt(i)) / 3.0) / Ut(i)
      Utx(i) = (2.0 * U(i) * Ux(i) + 2.0 * V(i) * Vx(i)) / 2.0 / Ut(i) * ds(i)
      Uty(i) = (2.0 * U(i) * Uy(i) + 2.0 * V(i) * Vy(i)) / 2.0 / Ut(i) * ds(i)
      Uts(i) = (U(i) / Ut(i) * Utx(i) + V(i) / Ut(i) * Uty(i))
      Kac(i) = mu / ru / Ut(i)**2.0 * Uts(i)
   end do

! ... Emperical Correlation Iterations ............................................'
   lanmt    = 0.0
   mt       = 0.0
   errormax = 0.0
   ni       = 0
   
   if (EmFor == "menter") then
      do
         ni = ni + 1
         do i = 1 , E
            if (tu(i)<0.027) then
               print*, "Error: Emperical Formulation For Transition Onset Crosses Tu's limit"
               pause
            end if
            mts = mt(i) 

            if (lanmt(i) <= 0.0) then
               Flmt = 1.0 + (12.986 * lanmt(i) + 123.66 * lanmt(i)**2.0 + 405.689 * lanmt(i)**3.0) &
                      * exp(-(Tu(i) / 1.5)**1.5)
            else if (lanmt(i) > 0.0) then
               Flmt = 1.0 + 0.275 * (1.0 - exp(-35.0 * lanmt(i))) * exp(-(2.0 * Tu(i)))
            end if

            if (Tu(i) <= 1.3) then
               Remt(i) = (1173.51 - 589.428 * Tu(i) + 0.2196 / Tu(i)**2.0) * Flmt
            else if (Tu(i) > 1.3) then
               Remt(i) = 331.50 * (Tu(i) - 0.5658)**(-0.671) * Flmt
            end if

            mt(i)      = Remt(i) * mu / ru / Ui
            lanmtnew(i)= ru * mt(i) * mt(i) * Uts(i) / mu
          
            if (abs(mts - mt(i)) > errormax) then
                errormax = abs(mts - mt(i))
                im = i
            end if

            if (Remt(i) < 20.0 .or. (lanmtnew(i) <= -0.1 .or. lanmtnew(i) >= 0.1)) then
                print*, "Error: Emperical Formulation For Transition Onset Crosses Remt's or Lanmt's limit"
                pause
                lanmt(i) = 0.0001 * lanmtnew(i) + lanmt(i)
            else
                lanmt(i) = lanmtnew(i)
            end if
        end do

        if (errormax <= 1.0e-10 .or. ni >= 25) exit
    end do
      
    else if (EmFor == "Sulu") then
       do
          ni = ni + 1
          do i = 1 , E
             if (Kac(i) <- 3.0e-6 .or. Kac(i) > 3.0e-6) then
                print*, "Error: Emperical Formulation For Transition Onset & 
                         Crosses Acceleration Parameter's limit"
                pause
             end if
          
             Flmt = 10.32 * lanmt(i) + 89.47 * lanmt(i)**2.0 + 265.51 * lanmt(i)**3.0
             Fac  = 0.0962 * Kac(i) * 10.0**6.0 + 0.148 * (Kac(i) * 10.0**6.0)**2.0 &
                    + 0.0141 * (Kac(i) * 10.0**6.0)**3.0
          
             if (lanmt(i) <= 0.0) then
                 Flmt = 1.0 + Flmt * exp(-Tu(i) / 3.0)
             else if (lanmt(i) > 0.0) then
                 Flmt = 1.0 + Fac * (1.0 - exp(-2.0 * Tu(i) / 3.0)) + 0.556 * & 
                        (1.0 - exp(-23.9 * lanmt(i))) * exp(tu(i) / 3.0)
             end if
             
             Remt(i)     = 803.73 * (Tu(i) + 0.6067)**(-1.027) * Flmt

             mt(i)       = Remt(i) * mu / ru / Ui
             lanmtnew(i) = ru * mt(i) * mt(i) * Uts(i) / mu
          
             if (abs(mts - mt(i)) > errormax) then
                errormax = abs(mts - mt(i))
                im = i
             end if

             if (Remt(i)<20.0 .or. (lanmtnew(i) <= -0.1 .or. lanmtnew(i) >= 0.1)) then
                 print*, "Error: Emperical Formulation For Transition Onset Crosses Remt's or Lanmt's limit"
                 pause
                 lanmt(i) = 0.0001 * lanmtnew(i) + lanmt(i)
             else
                 lanmt(i) = lanmtnew(i)
             end if
         end do

        if (errormax <= 1.0e-10 .or. ni >= 25) exit
      end do
   end if

   Return

End subroutine Tr_Emperical_Correlation
