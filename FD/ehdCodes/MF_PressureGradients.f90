

Subroutine MF_PressureGradients
   
Use NEnumber 

! ##############################################################################
! IN THE SUBROUTINE, PRESSURE GRADIENT IN X AND Y DIRECTION FOR ALL VOLUME ARE CALCULATED.
! THE GREEN'S THEOREM IS USED IN CALCULATING GRADIENT.
!	
! P : P*
! Pcor : CORRECTION OF P (P')
! B : EXPLICIT TERMS OF PRESSURE EQUATIONS
! anp & app : COEFFICIENT OF PARAMETERS (P* & P')
!
! ############################################################################## 


Implicit None

Integer       :: m1,m2

Real (Kind=8) :: dx,dy

   
   do i = 1 , E
      Px(i) = 0.0
      Py(i) = 0.0
      do j = 1 , 4
         select case (j)
            case(1)
               m1 = 1
               m2 = 2
            case(2)
               m1 = 2
               m2 = 3
            case(3)
               m1 = 3
               m2 = 4
            case(4)
               m1 = 4
               m2 = 1
         end select
        dx = x(Con(i,m2)) - x(Con(i,m1))
        dy = y(Con(i,m2)) - y(Con(i,m1))
        
        if(Neibr(i,j) /= 0) then   
           Px(i) = Px(i) + dy * ((1.0 - f(i,j)) * P(i) + f(i,j) * P(Neibr(i,j)))
           Py(i) = Py(i) - dx * ((1.0 - f(i,j)) * P(i) + f(i,j) * P(Neibr(i,j)))
!			Px(i)=Px(i)+dy*(P(i)+P(Neibr(i,j)))/2.0
!		    Py(i)=Py(i)-dx*(P(i)+P(Neibr(i,j)))/2.0
        else
!			if(FP(i,j)==2) then
!			   Px(i)=Px(i)+dy*(P(i)+P0)/2.0
!		       Py(i)=Py(i)-dx*(P(i)+P0)/2.0
!			else   
           Px(i) = Px(i) + dy * P(i)
           Py(i) = Py(i) - dx * P(i)
!           end if
        end if
      end do
   end do

   Return

End Subroutine MF_PressureGradients
