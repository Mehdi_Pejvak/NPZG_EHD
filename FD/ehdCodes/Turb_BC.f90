

Subroutine Turb_BC

Use NEnumber		

! ##############################################################################
! IN THE SUBROUTINE, BOUNDARY CONDITIONS FOR TURBULENCE MODEL'S EQUATION ARE APPLIED.
! ALL BOUNDARY CONDITIONS CAN BE FOUND IN MENTER PAPER [1].
!	
! Ktf : BOUNDARY CONDITIONS OF KINETIC ENERGY
! Ktf : BOUNDARY CONDITIONS OF OMEGA
! FP : LOCATION OF FACE IN THE DOMAIN 
!	   (1 --> PLATE SURFACE; 2 --> OUTLET; 3 --> FREE STREAM; 4 --> INLET; 7 --> UPSTREAM OF FLAT PLATE )

! --- REFFERENCES : ------------------
! [1]  : Menter, F. R., �Two-Equation Eddy-Viscosity Turbulence Models for Engineering Applications,� AIAA Journal, Vol. 32, No. 8, 1994, pp.
!		 1598-1605.
! ############################################################################## 

Implicit None


  do i=1,E
	do t=1,4
		if (FP(i,t)==1) then
			Ktf(i,t)=0.0	
			Omgf(i,t)=60.0*mu/(ru*beta1*wd(i)**2.0)
		else if (FP(i,t)==2) then
			Ktf(i,t)=4.0/3.0*Kt(i)-1.0/3.0*Kt(BNC(i,t))
			Omgf(i,t)=4.0/3.0*Omega(i)-1.0/3.0*Omega(BNC(i,t))
		else if (FP(i,t)==3) then
			Ktf(i,t)=Kt(i)	!Ktinf
			Omgf(i,t)=Omega(i)	!Omginf
		else if (FP(i,t)==4) then
			Ktf(i,t)=Kti
			Omgf(i,t)=Omegai
		else if (FP(i,t)==6) then		! Surface of Airfoil
			Ktf(i,t)=0.0
			Omgf(i,t)=60.0*mu/(ru*beta1*wd(i)**2.0)
		else if (FP(i,t)==7) then		! Symmetric Surface Upstream of the Flat Plate
			Ktf(i,t)=Kt(i)	!Ktinf
			Omgf(i,t)=Omega(i)	!Omginf
		end if
	end do
  end do


Return

End Subroutine Turb_BC