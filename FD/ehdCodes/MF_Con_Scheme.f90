

Subroutine MF_Con_Scheme

Use NEnumber

Implicit None

! ##############################################################################
! IN THE SUBROUTINE, VELOCITIES ON FACE FOR USING IN CONVECTION TERM OF NS 
! EQUATIONS ARE CALCULATED BASED ON DIFFERENT METHODS. FORMULAS ARE USED FROM  Versteeg & Malalasekra [1] 
! The processes of selection are done in Select_Scheme.fi function
!
! --- REFERENCES: ---------------
!
! [1] Versteeg & Malalasekra, "An Introduction to Computational Fluid Dynamics" 2nd Ed., PEARSON PUB., 2007. 
! ##############################################################################
   
Real (Kind = 8) :: Uu,Vu, &
                   RQu,RQv,Siu,Siv


  do i = 1,E
     do t = 1,4
        if(Neibr(i,t) /= 0) then
           if (BNC(i,t) /= 0) then
              if (AdU == 1.0) then
                 Uex(i,t) = (-1.0 * U(BNC(i,t)) - 2.0 * U(i) + 3.0 * U(FNC(i,t))) / 8.0	 ! QUICK
              else if (AdU == 2.0) then
                 RQu = (U(i) - U(BNC(i,t))) / (U(FNC(i,t)) - U(i))
                 Siu = max(z,min(2.0 * RQu,(3.0 + RQu) / 4.0,2.0))      ! QUICK Limiter
                 Uex(i,t) = Siu * (U(FNC(i,t)) - U(i)) / 2.0
              else if (AdU == 3.0) then
                 RQu = (U(i) - U(BNC(i,t))) / (U(FNC(i,t)) - U(i))
                 if (RQu > 0.0)  Siu = min(RQu,1.0)                     ! Mid-Mod Limiter
                 if (RQu <= 0.0) Siu = 0.0
                 Uex(i,t) = Siu * (U(FNC(i,t)) - U(i)) / 2.0
              else if (AdU == 4.0) then
                 RQu = (U(i) - U(BNC(i,t))) / (U(FNC(i,t)) - U(i))
                 Siu = max(z,min(2.0 * RQu,1.0),min(RQu,2.0))        ! SUPERBEE Limiter
                 Uex(i,t) = Siu * (U(FNC(i,t)) - U(i)) / 2.0
              else if (AdU == 5.0) then
                 RQu = (U(i) - U(BNC(i,t))) / (U(FNC(i,t)) - U(i))
                 if (RQu < 0.0 .or. U(FNC(i,t)) == U(i)) then
                    SiU = 0.0
                 else
                    Siu = (RQu + abs(RQu)) / (1.0 + RQu)    ! Van Leer Limiter
                 end if
                 Uex(i,t) = Siu * (U(FNC(i,t)) - U(i)) / 2.0
              end if

              if (AdV == 1.0) then
                 Vex(i,t) = (-1.0 * V(BNC(i,t)) - 2.0 * V(i) + 3.0 * V(FNC(i,t))) / 8.0 ! QUICK
              else if (AdV == 2.0) then
                 RQv = (V(i) - V(BNC(i,t))) / (V(FNC(i,t)) - V(i))
                 Siv = max(z,min(2.0 * RQv,(3.0 + RQv) / 4.0,2.0))   ! QUICK Limiter
                 Vex(i,t) = Siv * (V(FNC(i,t)) - V(i)) / 2.0
              else if (AdV == 3.0) then
                 RQv = (V(i) - V(BNC(i,t))) / (V(FNC(i,t)) - V(i))
                 if(RQv > 0.0)  Siv = min(RQv,1.0)                  ! Mid-Mod Limiter
                 if(RQv <= 0.0) Siv = 0.0
                 Vex(i,t) = Siv * (V(FNC(i,t)) - V(i)) / 2.0
              else if (AdV == 4.0) then
                 RQv = (V(i) - V(BNC(i,t))) / (V(FNC(i,t)) - V(i))
                 Siv = max(z,min(2.0 * RQv,1.0),min(RQv,2.0))      ! SUPERBEE Limiter
                 Vex(i,t) = Siv * (V(FNC(i,t)) - V(i)) / 2.0
              else if (AdV == 5.0) then
                 RQv = (V(i) - V(BNC(i,t))) / (V(FNC(i,t)) - V(i))
                 if (RQv < 0.0 .or. V(FNC(i,t)) == V(i)) then
                    Siv = 0.0
                 else
                    Siv = (RQv + abs(RQv)) / (1.0 + RQv)           ! Van Leer Limiter
                 end if
                    Vex(i,t) = Siv * (V(FNC(i,t)) - V(i)) / 2.0
              end if
           else
              Uex(i,t) = (U(FNC(i,t)) - U(i)) / 2.0
              Vex(i,t) = (V(FNC(i,t)) - V(i)) / 2.0
           end if
        end if
    end do
  end do


Return

End Subroutine MF_Con_Scheme
