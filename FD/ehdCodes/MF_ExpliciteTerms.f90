

Subroutine MF_ExpliciteTerms
   
Use NEnumber

Implicit None

! ##############################################################################
! IN THE SUBROUTINE, EXPLICITE TERMS OF NS EQUATION, INCLUDING TIME, DIFFUSION, 
! AND CONVECTION EXPLICIT TERMS, ARE CALCULATED.
! IN ADDITION, EHD BODY FORCE IS ALSO AS SOURCE TERM APPLIED IN EXPLICIT TERMS, 
! IN ORDER TO IMPLEMENT PLASMA ACTUATOR EFFECTS
!
! Uex & Vex : EXPLICIT TERM OF CONVECTION TERM OF NS
! Fb_x & Fb_y : EHD Body Force
! ##############################################################################


Integer         :: Nt,m1,m2
   
Real (Kind = 8) :: ConX,ConY


   CALL MF_NodalVelocities

   do i = 1,E
      ExpliciteX(i) = 0.0    !ru * ds(i)/dt*u(i)
      ExpliciteY(i) = 0.0    !ru * ds(i)/dt*v(i)
      do j = 1,4
         select case (j)
            case(1)
               m1 = 1
               m2 = 2
            case(2)
               m1 = 2
               m2 = 3
            case(3)
               m1 = 3
               m2 = 4
            case(4)
               m1 = 4
               m2 = 1
        end select
        if (Neibr(i,j) /= 0) then
           do t = 1,4
              if(i == Neibr(Neibr(i,j),t)) then
                Nt = t
                exit
              end if
           end do

           ConX = min(Z,massf(i,j)) * Uex(Neibr(i,j),Nt) + max(Z,massf(i,j)) * Uex(i,j)
           ConY = min(Z,massf(i,j)) * Vex(Neibr(i,j),Nt) + max(Z,massf(i,j)) * Vex(i,j)

           ExpliciteX(i) = ExpliciteX(i) + (mu+Mut(i)) * K(i,j)/M(i,j)*(Un(Con(i,m2)) - Un(Con(i,m1))) - ConX 
           ExpliciteY(i) = ExpliciteY(i) + (mu+Mut(i)) * K(i,j)/M(i,j)*(Vn(Con(i,m2)) - Vn(Con(i,m1))) - ConY 
         end if
      end do
   end do

Return

End Subroutine  MF_ExpliciteTerms
