

Subroutine Select_Scheme

Use NEnumber		

Implicit None
   

! @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

! IN THIS SUBROUTINE SCHEME OF CONVECTION TERM AND EXPERIMENTAL FORMULA 
! (Flenght & TRANSITION MOMENTUM THICKNESS RE) FOR TRANSITION MODEL ARE CHOSED.

! --- REFERENCES: ---------------

! [1] Versteeg & Malalasekra, "An Introduction to Computational Fluid Dynamics" 2nd Ed., PEARSON PUB., 2007. 

! @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


! ... Advection Schemes Describtion.........................................
!
! 1 : QUICK (3th-order) [1]
! 2 : TVD (QUICK Limiter) [1]
! 3 : TVD (Mid-Mod Limiter) [1]
! 4 : TVD (SUPERBEE Limiter) [1]
! 5 : TVD (MUSCL (Van Leer) Limiter) [1]
! 6 : TVD (Barth - Jespersen) [2]
! ..........................................................................

! ... Advection Schemes Setting ............................................

   AdU = 3
   AdV = 3
   AdK = 3
   AdO = 3
   AdI = 3
   AdR = 3

   print*,"... Advection Schemes"
   print*,"1 : QUICK (3th-order)"
   print*,"2 : TVD (QUICK Limiter)"
   print*,"3 : TVD (Mid-Mod Limiter)"
   print*,"4 : TVD (SUPERBEE Limiter)"
   print*,"5 : TVD (MUSCL (Van Leer) Limiter)"
   print*,"6 : TVD (Barth - Jespersen)"
   print*,"Advection Schemes For U: ", adU
   print*,"Advection Schemes For V: ", adV
   print*,"Advection Schemes For KE: ", adK
   print*,"Advection Schemes For OMEGA: ", adO
   print*,"Advection Schemes For INTERMITTENCY: ", adI
   print*,"Advection Schemes For Retheta: ", adR


! ... Correlation Function .................................................
!
! 1 : Malan (2009) [3]
! 2 : Menter (2009) [4]
! 3 : Sorensen (2009) [5]
! 4 : Suluksna [6]
! 5 : ONERA [7]
! ..........................................................................

! ... Correlation Function Setting .........................................

   CFS = 1

   print*,"... Correlation Function"
   print*,"1 : Malan (2009)"
   print*,"2 : Menter (2009)"
   print*,"3 : Sorensen (2009)"
   print*,"4 : Suluksna"
   print*,"5 : ONERA"
   print*," Choosed Correlation Function ", CFS

   print*, ".........................................................."

! ..........................................................................


   Return

End Subroutine Select_Scheme
