

Subroutine Tu_Strain_Vorticity

Use NEnumber

! ##############################################################################
! IN THE SUBROUTINE, VORTICITY AND STRAIN RATE OF FLUID FLOW FOR ALL CELLS BY 
! USING VALUE OF VELOCITY HAS BEEN OBTAINED IN THE PAST ITERATION ARE CALCULATED.
! FOR CALCULATION OF DEFRENTIATIONS, GAUSS'S THEROEM IS USED.
!	
! Vor : VORTICITY
! Str : STRAIN RATE
! Ux , Vx : DEFRENTIATION OF VELOCITY IN X DIRECTION
! Uy , Vy : DEFRENTIATION OF VELOCITY IN Y DIRECTION
! ############################################################################## 

Implicit None

Integer       :: m1,m2

Real (Kind=8) :: Strt11,Strt21,Strt22,Vort11,Vort12,Vort21,Vort22,dx,dy
				   
	Ux=0.0
	Uy=0.0
	Vx=0.0
	Vy=0.0
    do i=1,E
	  do j=1,4
         select case (j)
	       case(1)
	        m1=1
		    m2=2
           case(2)
   	        m1=2
		    m2=3
           case(3)
            m1=3
		    m2=4
		   case(4)
            m1=4
		    m2=1
         end select
         dx=x(Con(i,m2))-x(Con(i,m1))
	     dy=y(Con(i,m2))-y(Con(i,m1))
		 if(Neibr(i,j)/=0) then   
			Ux(i)=Ux(i)+dy*((1.0-f(i,j))*U(i)+f(i,j)*U(Neibr(i,j)))/ds(i)
		    Uy(i)=Uy(i)-dx*((1.0-f(i,j))*U(i)+f(i,j)*U(Neibr(i,j)))/ds(i)
			Vx(i)=Vx(i)+dy*((1.0-f(i,j))*V(i)+f(i,j)*V(Neibr(i,j)))/ds(i)
		    Vy(i)=Vy(i)-dx*((1.0-f(i,j))*V(i)+f(i,j)*V(Neibr(i,j)))/ds(i)
		 else
			Ux(i)=Ux(i)+dy*Uf(i,j)/ds(i)
		    Uy(i)=Uy(i)-dx*Uf(i,j)/ds(i)
			Vx(i)=Vx(i)+dy*Vf(i,j)/ds(i)
		    Vy(i)=Vy(i)-dx*Vf(i,j)/ds(i)
         end if
      end do
	  Strt11=0.5*(Ux(i)+Ux(i))
	  Vort11=0.5*(Ux(i)-Ux(i))
	  Strt12(i)=0.5*(Uy(i)+Vx(i))
	  Vort12=0.5*(Uy(i)-Vx(i))
	  Strt21=0.5*(Vx(i)+Uy(i))
	  Vort21=0.5*(Vx(i)-Uy(i))
	  Strt22=0.5*(Vy(i)+Vy(i))
	  Vort22=0.5*(Vy(i)-Vy(i))

	  Str(i)=sqrt(2.0*(Strt11*Strt11+Strt12(i)*Strt12(i)+Strt21*Strt21+Strt22*Strt22))
	  Vor(i)=sqrt(2.0*(Vort11*Vort11+Vort12*Vort12+Vort21*Vort21+Vort22*Vort22))
   end do
   

Return

End Subroutine Tu_Strain_Vorticity