Subroutine TranNodal
   
Use NEnumber

Implicit None

Integer:: m1,m2
				   

    Intmn=0.0
	Eff_intn=0.0
    Rehmtn=0.0
	Remtn=0.0
	do i=1,N
		do j=1,CV(i,1)
			Intmn(i)=Intmn(i)+Intm(CV(i,j+1))*f2(i,j)
			Eff_intn(i)=Eff_intn(i)+Eff_int(CV(i,j+1))*f2(i,j)
			Rehmtn(i)=Rehmtn(i)+Rehmt(CV(i,j+1))*f2(i,j)
			Remtn(i)=Remtn(i)+Remt(CV(i,j+1))*f2(i,j)
		end do
	end do

	do i=1,E
		do t=1,4
			select case (t)
	 		  case(1)
	 			m1=1
	 			m2=2
		  	  case(2)
	    		m1=2
	 			m2=3
		  	  case(3)
	 		 	m1=3
	 			m2=4
			  case(4)
	 		 	m1=4
	 			m2=1
		    end select

			if (FP(i,t)==4) then
				Intmn(Con(i,m1))=1.0
				Intmn(Con(i,m2))=1.0
				Rehmtn(Con(i,m1))=Remti
				Rehmtn(Con(i,m2))=Remti
			end if

			if (FP(i,t)==1 .or. FP(i,t)==2 .or. FP(i,t)==3.or. FP(i,t)==6 .or. FP(i,t)==7) then
				if (CV(Con(i,m1),1)/=1 .and. CV(Con(i,m2),1)/=1) then				
					Intmn(Con(i,m1))=(Intmf(CV(Con(i,m1),2),t)+ Intmf(CV(Con(i,m1),3),t))/2.0
					Intmn(Con(i,m2))=(Intmf(CV(Con(i,m2),2),t)+ Intmf(CV(Con(i,m2),3),t))/2.0
					Rehmtn(Con(i,m1))=(Rehmtf(CV(Con(i,m1),2),t)+Rehmtf(CV(Con(i,m1),3),t))/2.0
					Rehmtn(Con(i,m2))=(Rehmtf(CV(Con(i,m2),2),t)+Rehmtf(CV(Con(i,m2),3),t))/2.0
				else if (CV(Con(i,m1),1)==1) then
					Intmn(Con(i,m1))=Intmf(CV(Con(i,m1),2),t)
					Rehmtn(Con(i,m1))=Rehmtf(CV(Con(i,m1),2),t)
				else if (CV(Con(i,m2),1)==1) then
					Intmn(Con(i,m2))=Intmf(CV(Con(i,m1),2),t)
					Rehmtn(Con(i,m2))=Rehmtf(CV(Con(i,m1),2),t)					
				end if	
			end if				 
		end do
	end do

Return

End Subroutine TranNodal
