


Subroutine ERROR
   
Use NEnumbER

Implicit None

! ##############################################################################
! IN THE SUBROUTINE, ERROR FOR NS EQUATIONS ARE CALCULATED.
! MAXIMUM & AVRAGE ERROR FOR CONSERVATION OF MASS FLUX IN CELLS ARE CALCULATED.
! ##############################################################################

Real (Kind = 8) :: ER1
   ERmax = 0.0
   ER = 0.0

   do i = 1,E
      ER1 = 0.0
      
      do j = 1,4
         ER1 = ER1 + massf(i,j)
      end do
      
      ER1 = abs(ER1)
      
      if(ER1 > ERmax) then
         ERmax = ER1
         im = i 
      end if 
      
      ER = ER + ER1
   end do
   ER = ER/real(E)

Return
  
End Subroutine ERROR
