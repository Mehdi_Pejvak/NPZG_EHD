

Subroutine Tr_Rehmt_Explicit

Use NEnumber		

Implicit None
   
Integer      :: Nt,m1,m2

Real (Kind=8):: Rehmtex(E,4),Rmt,Simt,difRemt			   
				

  do i=1,E
	do t=1,4
		if(Neibr(i,t)/=0) then
			if (BNC(i,t)/=0) then
				if (AdR==1.0) 	then
					Rehmtex(i,t)=(-1.0*Rehmt(BNC(i,t))-2.0*Rehmt(i)+3.0*Rehmt(FNC(i,t)))/8.0	! QUICK
				else if (AdR==2.0) 	then
					Rmt=(Rehmt(i)-Rehmt(BNC(i,t)))/(Rehmt(FNC(i,t))-Rehmt(i))
					Simt=max(z,min(2.0*Rmt,(3.0+Rmt)/4.0,2.0))									! Quick Limiter
					Rehmtex(i,t)=Simt*(Rehmt(FNC(i,t))-Rehmt(i))/2.0
				else if (AdR==3.0) 	then
					Rmt=(Rehmt(i)-Rehmt(BNC(i,t)))/(Rehmt(FNC(i,t))-Rehmt(i))
					if(Rmt>0.0)  Simt=min(Rmt,1.0)												! Mid-Mod Limiter
					if(Rmt<=0.0) Simt=0.0
					Rehmtex(i,t)=Simt*(Rehmt(FNC(i,t))-Rehmt(i))/2.0
				else if (AdR==4.0) 	then
					Rmt=(Rehmt(i)-Rehmt(BNC(i,t)))/(Rehmt(FNC(i,t))-Rehmt(i))
					Simt=max(z,min(2.0*Rmt,1.0),min(Rmt,2.0))									! SUPERBEE Limiter
					Rehmtex(i,t)=Simt*(Rehmt(FNC(i,t))-Rehmt(i))/2.0
				else if (AdR==5.0) 	then
					Rmt=(Rehmt(i)-Rehmt(BNC(i,t)))/(Rehmt(FNC(i,t))-Rehmt(i))
					if (Rmt<=0.0 .or. Rehmt(FNC(i,t))==Rehmt(i)) then
						Simt=0.0
					else
						Simt=(Rmt+abs(Rmt))/(1.0+Rmt)											! Van Leer Limiter
					end if
					Rehmtex(i,t)=Simt*(Rehmt(FNC(i,t))-Rehmt(i))/2.0
				end if	 
			else
				Rehmtex(i,t)=(Rehmt(FNC(i,t))-Rehmt(i))/2.0
			end if			
		end if
	end do
  end do

  CALL TranNodal

   do i=1,E
      Rehmt_Exp(i)=0.0 !ru*ds(i)/dt*Rehmt(i)
	  do j=1,4
         select case (j)
	       case(1)
	        m1=1
		    m2=2
           case(2)
   	        m1=2
		    m2=3
           case(3)
			m1=3
		    m2=4
		   case(4)
			m1=4
		    m2=1
         end select

		if (Neibr(i,j)/=0) then
			do t=1,4
				if(i==Neibr(Neibr(i,j),t)) then
					Nt=t
					exit
				end if
			end do

			 difRemt=min(Z,massf(i,j))*Rehmtex(Neibr(i,j),Nt)+max(Z,massf(i,j))*Rehmtex(i,j)
			 Rehmt_Exp(i)=Rehmt_Exp(i)+sgmmt*(mu+Mut(i))*K(i,j)/M(i,j)*(Rehmtn(Con(i,m2))-Rehmtn(Con(i,m1)))-difRemt
		end if
	  end do
	end do

Return

End Subroutine Tr_Rehmt_Explicit