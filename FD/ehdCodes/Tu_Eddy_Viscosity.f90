

Subroutine Tu_Eddy_Viscosity

Use NEnumber

! ##############################################################################
! IN THE SUBROUTINE, TURBULENT VISCOSITY IS CALCULATED.
! IN THIS CALCULATION  Realizability Constraint of Durbin IS APPLIED TO CURE STAGNATION-POINT ANOMALY [1] 
!	
! ERROR=SIGMA (Un-Un-1)/SIGMA(AP*Un)	
! OMEGAnew: VALUE OF CURRENT STEP	
! OMEGA : VALUE OF PREVIOUS STEP AND IN FINAL STEP, VALUE OF CURRENT ONE BY APPLYING UNDERRELAXATION FACTOR
! SIGMA : SUM OF ALL CELLS' VALUE

! --- REFFERNCES: ----------------
! [1] P. A. Durbin, B. A. Pettersson Reif, "statistical theory and modeling for  turbulent flows", 2nd ed., John Willy pub., 2011.
!
! ############################################################################## 

Implicit none

Real (Kind=8) :: Vis1,Vis2,Vis,RC
				   

Logical       :: log1


	do i=1,E
		log1=((FP(i,1)==3 .or. FP(i,2)==3 .or. FP(i,3)==3 .or. FP(i,4)==3) .or. &
			  (FP(i,1)==4 .or. FP(i,2)==4 .or. FP(i,3)==4 .or. FP(i,4)==4) .or. &
			  (FP(i,1)==7 .or. FP(i,2)==7 .or. FP(i,3)==7 .or. FP(i,4)==7))

		if (log1) then
			Mut(i)=Rmu*mu
		else  
			Vis1=1.0/Omega(i)
			Vis2=a1/Str(i)/Ft2(i)
			Vis=min(Vis1,Vis2)
			
			RC=0.6/sqrt(3.0)/Str(i)		! Realizability Constraint of Durbin
			Mut(i)=ru*Kt(i)*min(Vis,RC)
		end if
	end do


Return

End Subroutine Tu_Eddy_Viscosity