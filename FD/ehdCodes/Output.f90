

Subroutine Output(i2)
   
Use NEnumber

Implicit None

Integer :: i2

! ... OUTPUT FILES ..............................................................

   Open (40,file=".\DATA\T3C4\Process-T3C4-12k-PSTW.txt")
   Open (30,file=".\DATA\T3C4\Flow-T3C4-12k-PSTW.plt")
   Open (50,file='.\DATA\T3C4\Error-T3C4-PSTW.plt')
   Open (60,file='.\DATA\T3C4\PostProcess-T3C4-12k-PSTW.plt')
   Open (70,file='.\DATA\T3C4\WSS-T3C4-12k-PSTW.plt')
   Open (80,file='.\DATA\T3C4\RS-T3C4-12k-PSTW.plt')
   Write(30,'(11A)')'VARIABLES = "X", "Y", "U", "V", "P" , "KE" , "Omega", "Intm" , "EffIntm" ,"Rehmt" ,"Remt"'
   Write(30,*)'ZONE N=',N,', E=',E,', C=RED, F=FEPOINT, ET=QUADRILATERAL' 
   if (i2 == 1) Write(50,*) 'VARIABLES = "iter" , "Ermax" , "ErU" , "ErV" , "ErK" , "ErO" , "ErI" , "ErR" '
   Write(50,'(I9,7E24.5)') i2, ermax, ErrorU, ErrorV, Errork, ErrorO, ErrorI, ErrorR
   
   if (mod(i2,1000) == 0) then
   Open (90,file = '.\DATA\cellvalue.dat')
   
      do i = 1 , E
         Write(90,'(9E24.5)') U(i), V(i), P(i), Kt(i), Omega(i), Mut(i), Intm(i), Rehmt(i), Remt(i)
      end do
   end if

   Close(90)

21 Format(11E24.5)

   do i = 1 , N
      Write(30,21) x(i), y(i), un(i), vn(i), Pn(i), Ktn(i), Omegan(i), Intmn(i), Eff_intn(i), Rehmtn(i), Remtn(i)
   end do

22 Format(4I7)
   
   do i = 1 , E
      Write(30,22) Con(i,1:4)
   end do
   
   Close(30)
   
   Write(80,'(3A)')'VARIABLES = "X", "Y", "Re-Stress"'
   Write(80,*)'ZONE t="ZONE 1"I=',n1,', J=',n2
   
23 Format(3E24.5)
   do i = 1 , E
      Write(80,23) x(i), y(i), 2.0 * Mut(i) * Strt12(i)
   end do
   Close (80)
        
   call Y_Plus

    Write(40,*) " Number Of Nodes", N, "    Number Of Cells=", E
    Write(40,*) " Number of Node in Boundary",n1,n2,n3,n4,n6
    Write(40,*) " alpha P=",alphap,",  alpha V=",alphav
    Write(40,*) " alpha KE=",alphaK,",  alpha Omega=",alphaO
    Write(40,*) " alpha Int=",alphaI,",  alpha ReThetaT=",alphaR
    Write(40,*) " ------Advection Scheme ---------------------------------------"
    Write(40,*) " 1 : QUICK (3th-order)"
    Write(40,*) " 2 : TVD (QUICK Limiter)"
    Write(40,*) " 3 : TVD (Mid-Mod Limiter)"
    Write(40,*) " 4 : TVD (SUPERBEE Limiter)"
    Write(40,*) " 5 : TVD (Van Leer (MUSCL) Limiter)"
    Write(40,*) " 6 : TVD (Barth - Jespersen)"
    Write(40,*) " Advection Schemes in U: ", AdU
    Write(40,*) " Advection Schemes in V: ", AdV
    Write(40,*) " Advection Schemes in KE: ", AdK
    Write(40,*) " Advection Schemes in Omega: ", AdO
    Write(40,*) " Advection Schemes in Int.: ", AdI
    Write(40,*) " Advection Schemes in Rehmt: ", AdR
    Write(40,*) " ------ Correlation Function ---------------------------------------"
    Write(40,*) " 1 : Malan (2009)"
    Write(40,*) " 2 : Menter (2009)"
    Write(40,*) " 3 : Sorensen (2009)"
    Write(40,*) " 4 : Suluksna"
    Write(40,*) " 5 : ONERA"
    Write(40,*) " Correlation Function:", CFS
    Write(40,*) "-------Inlet Values of Flow -----------------------------------"
    Write(40,*) " Reynolds Number=",Re
    Write(40,*) " Inlet Velocity=" , Ui
    Write(40,*) " Boundary Layer Thickness In Outlet=",0.37*Lc/(Re**0.2)
    Write(40,*)'  Inlet Turbulence Intensity=', Tui, '%'
    Write(40,*)'  Turbulence Intensity At Leading Edge =',TuL*100.0, '%'
    Write(40,*)'  Viscosity Ratio (mut/mu)=  ', Rmu
    Write(40,*)'  Inlet Kinetic Energy = ', Kti
    Write(40,*)'  Inlet Dissipation Per Turbulent Kinetic Energy= ' ,Omegai
    Write(40,*)'  Inlet Intermittency= ', Intmi
    Write(40,*)'  Inlet Transition Onset Momentum Thickness Re= ', REmti   
    Write(40,*) "--------- Results -----------------------------------------------"
    Write(40,*) " Number of Iterations:",i2
    Write(40,*) " Operation Time=",op_time
    Write(40,*) " "
    Write(40,*) " Mass Ratio:",m_ratio
    Write(40,*) " Min. Y+ In First-Row Cells=", Y_Pmin
    Write(40,*) " Max. Y+ In First-Row Cells=", Y_Pmax
    Write(40,*) " Cell with Max. Y+ In First-Row =",cell_maxYP
    Write(40,*) " Position Of Turbulent Parameter On Plate=",Xp(sp)
    if (pos/=0) Write(40,*) " Begining of Separation:",Xp(POS)
    Write(40,*) " "
    Write(40,*) " Average Error:",er," , Max. Error:",ermax
    Write(40,*) " Error U:",ErrorU," , Error V:",ErrorV
    Write(40,*) " Error KE:",Errork," , Error Omega:",ErrorO
    Write(40,*) " Error Intm:",ErrorI," , Error Rehmt:",ErrorR
    Write(40,'(A,/,2F7.3,/,2F7.3,/,2F7.3,/,2F7.3,/)')"    x      y",x(Con(im,1)), y(Con(im,1)), &
                                                                    x(Con(im,2)), y(Con(im,2)), & 
                                                                    x(Con(im,3)), y(Con(im,3)), &
                                                                    x(Con(im,4)), y(Con(im,4)) 
   
   Close(40)
   
   Return

End Subroutine Output
