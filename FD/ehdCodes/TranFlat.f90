
!Include 'NEnumber.f90'

Program TranFlat

!... Main Program ..................................................................

Use NEnumber
!Use portlib

! ##############################################################################
! THIS PROGRAM IS DEVELOPED TO NUMERICALLY MODELS LOW REYNOLDS NUMBER FLOW INCLUDED TURBULENCE AND TRANSITION REGIMES.
! IN MODELING NS EQUATION SIMPLER, AND MOMENTUM INTERPOLATING METHOD ARE USED [1]
! IN MODELING TURBULENCE, SST-KW IS USED [2]
! IN MODELING TRANSITION, GAMMA_ReTETHA CORELATION BASED TRANSITION METHOD IS USED [3]
!
! --- REFFERENCES : ------------------
! [1]	: Cheng Y. P., Lee T. S., Low H. T. and Tao W. Q., Improvement of SIMPLER algorithm for incompressible flow on collocated grid system. 
!		Numerical Heat Transfer, Part B, vol.51(5), pp. 463-486, 2007.
! [2]	: Menter, F. R., Two-Equation Eddy-Viscosity Turbulence Models for Engineering Applications, AIAA Journal, Vol. 32, No. 8, 1994, 
!		pp.1598-1605.
! [3]	: Langtry, R.B., A Correlation-Based Transition Model Using Local Variables for Unstructured Parallelized CFD Codes,
!       Dr.-Ing thesis, Institute of Thermal Turbomachinery and Machinery Laboratory, University of Stuttgart, 2006.
!
! ##############################################################################

Implicit None

Integer :: NOI,NOIT,ik

Real (kind=8):: tel,dft, &
                Fbt_x(E),Fbt_y(E)

Character:: CI

Common /PBFt/ Fbt_x,Fbt_y
! .................................................................................

  !op_time=timef()

  Print*, ">>> Reading Grid Data and Setting Initial Conditions and Properties ..."
  Call Geometry
  Call Property   
  Call Select_Scheme
  print*," The point in which results are calculated upward:",xp(sp)
  Call Initial
    
  m_ratio=1.0
  tel=1.0e-7
  
   T_Time=0.0
   T_Time=T_Time+dt
   NOI=0
   NOIT=5
   dft=1.0/NOIT/3.0
   t=1

Do 
    do ik=1,E
         Fb_x(ik)=Fbt_x(ik)*t*dft
         Fb_y(ik)=Fbt_y(ik)*t*dft
         ST_x(ik)=Fb_x(ik)*ds(ik)
         ST_y(ik)=Fb_y(ik)*ds(ik)
    end do
   Do 
! ... Navier-Stocks Equations Part ..............................................................

      NOI=NOI+1

	  Call MF_BC
      Call MF_Coefficients 
	  Call MF_PressureGradients
      Call MF_MassFlux
	  Call MF_Con_Scheme
      Call MF_ExpliciteTerms   
      Call MF_Velocity(1)

      Call MF_Pressure(1)

	  Call MF_PressureGradients
      Call MF_Velocity(2)

	  Call MF_BC
      Call MF_MassFlux
	  Call MF_Con_Scheme
      Call MF_Coefficients 
      Call MF_Pressure(2)

	  Call MF_PressureGradients
      Call MF_Corrections
	  Call MF_BC
      Call MF_MassFlux

! ... Turbulent Equations Part ....................................................

	  Call Tu_Strain_Vorticity
	  Call Turb_BC
	  Call Tu_KO_Gradient
	  Call Tu_SST_Fun
	  Call Turb_Prm	
	  Call Tu_Omega_Explicit
	  Call Tu_KE_Explicit
	  Call Turb_Source
	  Call Turb_Dis
	  Call Tu_Prod_CD
	  Call Turb_Coef
	  Call Tu_DPTKE
	  Call Tu_KE
	  Call Tu_Eddy_Viscosity

! ... Transition Equations Part ...................................................

	  Call Tr_Emperical_Correlation
	  Call Tran_BC
	  Call Tr_Exp_Form
	  Call Tr_Int_Prod
	  Call Tr_Int_Des
	  Call Tr_Rehmt_Prod
	  Call Tr_Int_Explicit
	  Call Tr_Rehmt_Explicit
	  Call Tran_Coef
	  Call Tr_Intermittency
	  Call Tr_MTRe
	  Call Tr_EffInt

	  if(mod(NOI,100)==0 .or. NOI<=10) then 
		 
		 Call Error
         if (mod(NOI,100)==0)Call Output(NOI)
		 if (mod(NOI,500)==0) Call PostProcessing
         
	     Print '(A,I6,A,E24.15)',">>> Number of Iterations:",NOI," , Mass ratio:",m_ratio
		 Print*, ' ... Errors: '
		 Print*," Mass Flux: >> Ave.:",er,    " ,  Max.:",ermax
		 Print*," Velocity   >>    U:",ErrorU," ,     V:",ErrorV
		 Print*," Turbulence >>    K:",Errork," , Omega:",ErrorO
		 Print*," Transition >> Intm:",ErrorI," ,  Rett:",ErrorR
	     Print '(A,/,2F7.3,/,2F7.3,/,2F7.3,/,2F7.3,/)',"    x      y",x(Con(im,1)),y(Con(im,1)), &
																	  x(Con(im,2)),y(Con(im,2)), &
																	  x(Con(im,3)),y(Con(im,3)), &
																	  x(Con(im,4)),y(Con(im,4))
		 !Pause	   
	  end if

	  if(ermax<tel .and. ErrorU<tel .and. ErrorV<tel .and. ErrorK<tel .and. ErrorO<tel .and. ErrorI<tel .and. ErrorR<tel) Exit
      if(mod(NOI,4000)==0 .and. t<10)   go to 133
   End do
 t=t+1
133   Call Y_Plus
    
!  if(T_Time>=10.0) exit
 End do
 !op_time=timef()
 Call Output(NOI)
 Call PostProcessing
 !Print*,">> Process Time=",op_time
 !Print*,">> Process Speed (IPS)=",  NOI/op_time
pause

Contains

!... Subroutines ..................................................................

Include 'MF_Coefficients.f90'
Include 'MF_Corrections.f90'
Include 'Error.f90'
Include 'Select_Scheme.f90'
Include 'MF_ExpliciteTerms.f90'
Include 'Geometry.f90'
Include 'Initial.f90'
Include 'MF_MassFlux.f90'
Include 'Output.f90'
Include 'MF_Pressure.f90'
Include 'MF_PressureGradients.f90'
Include 'MF_Velocity.f90'
Include 'MF_NodalVelocities.f90'
Include 'MF_Con_Scheme.f90'
Include 'MF_BC.f90'
Include 'PostProcessing.f90'
Include 'Property.f90'

Include 'Tu_Strain_Vorticity.f90'
Include 'Tu_SST_Fun.f90'
Include 'Turb_Prm.f90'
Include 'Tu_KO_Gradient.f90'
Include 'Turb_BC.f90'
Include 'Tu_Omega_Explicit.f90'
Include 'Tu_KE_Explicit.f90'
Include 'Turb_Dis.f90'
Include 'Turb_Source.f90'
Include 'Tu_Prod_CD.f90'
Include 'Turb_Coef.f90'
Include 'Tu_DPTKE.f90'
Include 'Tu_KE.f90'
Include 'Tu_Eddy_Viscosity.f90'
Include 'TurbNodal.f90'
Include 'Y_Plus.f90'

Include 'Tr_Emperical_Correlation.f90'
Include 'Tran_BC.f90'
Include 'Tr_Exp_Form.f90'
Include 'Tr_Int_Prod.f90'
Include 'Tr_Int_Des.f90'
Include 'Tr_Rehmt_Prod.f90'
Include 'Tr_Int_Explicit.f90'
Include 'Tr_Rehmt_Explicit.f90'
Include 'Tran_Coef.f90'
Include 'Tr_Intermittency.f90'
Include 'Tr_MTRe.f90'
Include 'Tr_EffInt.f90'
Include 'TranNodal.f90'
!...................................................................................

End Program TranFlat
