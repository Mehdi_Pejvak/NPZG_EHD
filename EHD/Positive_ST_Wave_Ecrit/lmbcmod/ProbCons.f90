
!__________________________________________________________________________________

    Module ProbCons

	integer:: NF=700, &  ! Number of Terms in the Fourier Transformation
	          NT=30		 ! Number Time of Divisions of a Half Priod

	real:: Eps0=8.854e-12, &	  ! 
		   Eps=1.0, &			  !
	       Landa=2.54e-5, &		  ! Debye Length - Unit: Meters
             V0=12.0e3, Vi=2.0e3, &  ! Unit: Volts
             Eb=30.0e5, &			  ! Breakdown Electric Field Strength - Unit: V/m
		   rua=300., &			  ! Effective Resistivity of Air - Unit: Columbs per Cubic Meters
		   f=3.0e3 , &			  ! Frequncy - Unit: Hz
		   hpn=1.0e-3, &          ! Height of Subcircuit - Unit: Meters
		   zn=7.6e-2, &			  ! Depth of Subcircuit - Unit: Meters
		   W=0.99242203E-01, &    ! Length of Encapsulated Electrode - Unit: Meters
		   Ld=0.127e-3, &		  ! Thickness of Dielecric (Fig. 4.12) - Unit: Meters
		   t=0.102e-3, &		  ! Thickness of Electrodes - Unit: Meters
		   epsd=2.8, &			  ! Dielectric Permittivity - Without Unit (Dimentionless)
		   epsa=1.0, &			  ! Air Permittivity - Without Unit (Dimentionless)
		   
		   alpha=1.0			  ! Under-Relaxation Factor

	Integer, parameter :: IOR=150  ! IOR : Number of Iterations Deligated to Gauss-Seidel Method

	Real (Kind=8),parameter :: Tol_abs=1.0e-5 ! Tollerence
    
	Complex :: j=(0,1)

!	TO ILLUSTRATE THE GEOMETRY OF CIRCUIT, LOOK AT THE ORLOV THESIS, P. 108-111
    

    ! Parameters for Geometry
    integer:: N,E, &
                  m0,m10,m11,m12,m13,m14,Ktotal, &
                          mr=1,itr_max=1, &
                          NCI

        ! N: Number of Geometry Grid Points
        ! E: Number of Geometry Grid Elements
    ! m0: Number of boundary divisions in the left side of domain
    ! m10,m11,m12,m13,m14: Number of boundary divisions in the Lower side ofdomain
    ! m2: Number of boundary divisions in the right side of domain
    ! m3: Number of boundary divisions in the upper side of domain
        ! Ktotal: Total number of time steps
        ! NCI : No. of Cell on Virtual Electrode Which is Boundary Of Ionized and Non-Ion>


    integer,allocatable:: Con(:,:),Sn(:),Se(:,:),Neib(:,:),NE(:,:),FP(:,:)
    ! Con: Connectivity Matrix.
    !  Se: Array which determines the status of each of the element faces
        !      with respect to the boundaries. Its values can be 1,2,3,4 or 5
        !      for the boundary faces, and 0 for the internal faces.
    !  Sn: Array which determines the status of each of the nodes with
        !      respect to the boundaries, the same as Se.
        !neib: Elemnt Neighbours.
        !  NE: All Elements each Node belongs to them.
        !  FP: Array which determines Position of each of the faces of cells
        !      rgarding to the boundaries.

        real:: H,L0,L1,L2,L3,L4,Tl
    ! H,L0,L1,L2,L3,L4: Dimention of Boundary Portions in meters
        ! Tl: Value of the Half Priod of Potential Oscillations

        Real (Kind=8):: er
    !   er: Max. relative error in the iteration
        ! Vout: Applied Electrical Potential on the Surface of Exposed Electrode


        real,allocatable:: x(:),y(:),xp(:),yp(:), &
                           GP(:,:,:),A(:), &
                           P_nodal(:,:)
    !          x,y: Coordinates of Geometry Grid Points
        !           GP: Array Contains Geometry Properties K,L,M, from the file GridChara>
        !               Its first index is for the Element Number,
        !               the second one is for the face of element (face 1,2 or 3) and
        !               in the third index position, number 1 is for K or d1,
        !               number 2 is for L or s1, and number 3 is for M.
    !            A: Element Areas
        ! P_nodal(:,:): Electrical Potential at the position of nodes

        Real (Kind=8),allocatable::     P(:), Pv(:),Vout(:),Explicit(:), &
                                        Ieff(:,:), Iptot(:), Fb_X(:,:), &
                                        Fb_Y(:,:), Fbt_X(:), Fbt_Y(:), Pt(:)
        !   P: Electrical Potential of Element Centres
        !   Pv: Electrical Potential on the Surface of Virtual Electrode
        !   Explicit : Explicit Term of Potential Equation
        !   Ipt : Plasma Current
        !       Fb  : Plasma Body Force
        !       Fbt : Time-Averaged Plasma Body Force


! Important Note: All of the above allocatables are allocated
        !                 in the GeomInput subroutine

    
    End Module ProbCons

