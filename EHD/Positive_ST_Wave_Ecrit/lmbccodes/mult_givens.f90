

 Subroutine mult_givens (c, s, k, g )

!********************************************************************************
!!  MULT_GIVENS applies a Givens rotation to two successive entries of a vector.
!********************************************************************************

 implicit none
 Real (Kind=8) :: c,g(1:m+1),g1,g2,s
 Integer :: k

 g1 = c * g(k) - s * g(k+1)
 g2 = s * g(k) + c * g(k+1)

 g(k)   = g1
 g(k+1) = g2

 Return
 End Subroutine mult_givens
