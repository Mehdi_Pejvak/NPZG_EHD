

 Subroutine Body_Force (k)

! Use Geom
 Use ProbCons

 Implicit None

 Real (Kind=8) :: Pf,dX,dY,Grad_Pot_X,Grad_Pot_Y,pi,eps0,Fbt
 Integer :: i,jj,m1,m2,k,ll

 pi=4.0*atan(1.0)
 eps0=1.0e-9/36.0/pi

 fb_x(1:E,k)=0.0
 fb_y(1:E,k)=0.0
 fbt_x=0.0
 fbt_y=0.0
 fbt=0.0
 Grad_Pot_X=0.0
 Grad_Pot_Y=0.0


 Do i=1,E

	if (Xp(i)>X(m10+m11+m12) .and. Xp(i)<X(NCI+m10+m11+m12-1)) then 
      
 ! ... CALCULATION OF POTENTIAL GRADIENT ...................................................

		Grad_Pot_X=0.0
		Grad_Pot_Y=0.0

		Do ll=1,4
			select case(ll)
				case(1)
					m1=1
					m2=2
				case(2)
					m1=2
					m2=3
				case(3)
					m1=3
					m2=4
				case(4)
					m1=4
					m2=1
			end select 
			dX=x(Con(i,m2))-x(Con(i,m1))
			dY=y(Con(i,m2))-y(Con(i,m1))
   
			if(Se(i,ll)==0) Pf=(P(i)+P(neib(i,ll)))/2.0	! No Boundary, Internal Face
			if(Se(i,ll)==1 .or. Se(i,ll)==2) Pf=0.0		! Left and Right Sides
			if(Se(i,ll)==3) Pf=P(i)			! Lower Side, Without Electrode
			if(Se(i,ll)==4) Pf=Vout(k)		! Lower Side, Real Electrode
			
			if(Se(i,ll)==5) then		! Lower Side, Virtual Electrode
				Pf=(Pv(Con(i,m1))+Pv(Con(i,m2)))/2.0
			end if
			Grad_Pot_X=Grad_Pot_X+Pf*dY
			Grad_Pot_Y=Grad_Pot_Y-Pf*dX
				 
		end do
		
		Grad_Pot_X=Grad_Pot_X/A(i)
		Grad_Pot_Y=Grad_Pot_Y/A(i)

! ..............................................................................................

		if (Iptot(k)>0.0) then
			Fb_X(i,k)=-abs(p(i)*Grad_Pot_X*Eps0/Landa**2.0)	!*exp(-(Xp(i)-X(m10+m11+m12))/Ld))
			Fb_Y(i,k)=p(i)*Grad_Pot_Y*Eps0/Landa**2.0	    !*exp(-(Xp(i)-X(m10+m11+m12))/Ld)
		else if (Iptot(k)<=0.0) then
			Fb_X(i,k)=abs(p(i)*Grad_Pot_X*Eps0/Landa**2.0)	!*exp(-(Xp(i)-X(m10+m11+m12))/Ld))
			Fb_Y(i,k)=-abs(p(i)*Grad_Pot_Y*Eps0/Landa**2.0)	!*exp(-(Xp(i)-X(m10+m11+m12))/Ld)
		end if
	end if

 end do

 if (k==ktotal) then
	do i=1,E
		do jj=1,ktotal
			Fbt_X(i)=Fbt_X(i)+Fb_X(i,jj)/Ktotal
			Fbt_Y(i)=Fbt_Y(i)+Fb_Y(i,jj)/Ktotal
		end do
		Fbt=Fbt+sqrt(Fbt_X(i)**2.0+Fbt_Y(i)**2.0)
	end do
	fbt=fbt/E
	Print*, "Total Body Force=",Fbt
 end if


 Return

 End Subroutine Body_Force
