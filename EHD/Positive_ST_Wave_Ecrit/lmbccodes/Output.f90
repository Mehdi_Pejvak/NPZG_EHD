
!__________________________________________________________________________________

    subroutine Output

!    use Geom
	use Probcons

    implicit none

    Integer:: i,k
    

    open (20,file='C:\MY FOLDER\Project\Code\CFD\Flat Plate\EHD2CFD\EHD\Positive ST Wave_Ecrit\Data\Enc_Pot-12v-3h-zpg.plt')
    open (30,file="C:\MY FOLDER\Project\Code\CFD\Flat Plate\EHD2CFD\EHD\Positive ST Wave_Ecrit\Data\ElecSol-12v-3h-zpg.plt")
    open (40,file='C:\MY FOLDER\Project\Code\CFD\Flat Plate\EHD2CFD\EHD\Positive ST Wave_Ecrit\Data\Current-12v-3h-zpg.plt')
    open (50,file='C:\MY FOLDER\Project\Code\CFD\Flat Plate\EHD2CFD\EHD\Positive ST Wave_Ecrit\Data\Body_Force-12v-3h-zpg.plt')
    open (60,file='C:\MY FOLDER\Project\Code\CFD\Flat Plate\EHD2CFD\EHD\Positive ST Wave_Ecrit\Data\Total_Current-12v-3h-zpg.plt')
   
	13 format (4E25.6)
	14 format (2E25.6)
	15 format (13E25.6)
    18 format (E25.6,I8,E25.6)
	21 format (3E18.8)
	22 format (4I8)
	23 format (1E18.8)

	write(30,'(A37)')'TITLE = "LECM Potential Distribution"'
	write(30,'(A25)')'VARIABLES = "X", "Y", "P"'


	do k=1,ktotal
        if(k==1) then
			write(30,'(A9,I1,A4,I6,A4,I6,A26)')' ZONE T=Z',k,', N=',N,', E=',E,', DATAPACKING=POINT, C=RED'  
			write(30,'(A25)')' ZONETYPE=FEQUADRILATERAL'
			do i=1,N
				write(30,21) x(i),y(i),P_nodal(i,k)
			end do
			do i=1,E
				write(30,22) Con(i,1:4)
			end do
        else
			if(k<=9) then
				write(30,'(A9,I1,A4,I6,A4,I6,A26)')' ZONE T=Z',k,', N=',N,', E=',E,', DATAPACKING=POINT, C=RED'  
			else if(k<=99) then
				write(30,'(A9,I2,A4,I6,A4,I6,A26)')' ZONE T=Z',k,', N=',N,', E=',E,', DATAPACKING=POINT, C=RED'  
			else if(k<=999) then
			    write(30,'(A9,I3,A4,I6,A4,I6,A26)')' ZONE T=Z',k,', N=',N,', E=',E,', DATAPACKING=POINT, C=RED'
			end if

			write(30,'(A79)')' ZONETYPE=FEQUADRILATERAL, VARSHARELIST = ([1, 2]=1), CONNECTIVITYSHAREZONE = 1'

			do i=1,N
				write(30,23) P_nodal(i,k)

			end do
		end if

		write (20,15) (1./f)/NT*k,Vout(k),P_nodal(m10+m11+m12+1,k),  P_nodal(m10+m11+m12+17,k),P_nodal(m10+m11+m12+34,k),P_nodal(m10+m11+m12+51,k), &
						P_nodal(m10+m11+m12+68,k),P_nodal(m10+m11+m12+85,k),P_nodal(m10+m11+m12+102,k),P_nodal(m10+m11+m12+119,k), &
						P_nodal(m10+m11+m12+136,k),P_nodal(m10+m11+m12+153,k),P_nodal(m10+m11+m12+168,k)
	
	end do

	write(40,'(A37)')'TITLE = "Plasma Extension & Current"'
	write(40,'(A35)')'VARIABLES =  "X", "Time", "Current"'
	WRITE(40,*), 'ZONE t="ZONE 1"I=',Ktotal,'J=',m13

	Do i=1,m13
		do k=1,ktotal
			Write (40,18) (X(i+m10+m11+m12)-X(m10+m11+m12))/W,K-1,Ieff(i,k)
		end do
	end do

	write(50,'(A37)')'TITLE = "Plasma Body Force"'
	write(50,'(A37)')'VARIABLES = "Xp", "Yp", "Fbx","Fby"'
	WRITE(50,*), 'ZONE t="ZONE 1 ,"i=',m10+m11+m12+m13+m14,',j=',m0

	Do i=1,E
		Write (50,13) Xp(i),Yp(i),Fbt_x(i),Fbt_y(i)
	end do

	write(60,'(A37)')'TITLE = "Current in Each Time"'
	write(60,'(A37)')'VARIABLES = "Time", "I"'

	Do i=1,Ktotal
		Write (60,14) (1./f)/NT*i,Iptot(i)
	end do

	close (20)
	close (30)
	close (40)
	close (50)
	close (60)

	
	Return
    End Subroutine Output
