
!__________________________________________________________________________________

    Function LMBC(m,Time1,deltaT,k)

! ##############################################################################
! IN THE FUNCTION BY USING LUMPED ELEMENT METHOD INTRODUCED BY ORLOV, 
! POTENTIAL VALUE ON THE SYRFACE OF ENCAPSULATED ELECTRODE ARE CALCULATED. 
! THIS VALUES, THEN, ARE USED AS BOUNDARY CONDITION IN SOLVING THE POTENTIAL EQUATION
! ##############################################################################

    use Geom
    use ProbCons
     
    implicit none

    integer:: m,ni,nj,mf,k,ttt
    real:: pi,eps0,Time1,dn,hn,omega,Ln,Can,Cdn,Rn,deltaT
	Real (Kind=8) :: LMBC, & ! LMBC: Lumped Method Baundary Condition for Potential on the Surface of Virtual Electrode
	                 Vapp1,V1,V01,Vapp,Phiv, &
					 dv2,dv1,dv,Ip,Ip1, &
					 Rz,Iz,Rv,Iv,Ri,Ii,I1
	Complex:: Z2,Z3,Z,C,Z1,Z12
	
	pi=4.0*atan(1.0)
	eps0=1.e-9/36.0/pi

	dn=L3/m13
	hn=hpn/m13
	nj=m13
	
	LMBC=0.0
!	Vapp=0.				! FOR SQUARE WAVE
!	mf=1

	Vapp=0.0	!V0/2.0			! CAUTION >>> IT IS FOR SAW WAVE - IT SHOULD BE MODIFIED FOR OTHER WAVE FORMS
	mf=0

	V01=0.0

	Ln=sqrt(t*t+(L3*real(m)/real(m13))*(L3*real(m)/real(m13)))		! Distance Over the Dielectric Surface (Orlov's Thesis, P. 108, Fig. 4.9)
	Can=eps0*epsa*zn*hn/Ln		! Air Capacitor
	Cdn=eps0*epsd*zn*dn/Ld		! Dielectric Capacitor
	Ieff(m,k)=0.0
        
	Fourier_L: do		! For the Fourier Transformation Terms and Calculation of ODE Equation (Equ. ) By Using Fourier Transformation.
	I1=0.0

	! ...... SQUARE WAVE ....................................................................................
!		Omega=mf*pi/Tl                 !    1	 Frequency of Wave in Fourier Series	
!		if(mod(mf,2)==0.0) then        !    2
!			V01=0.0                    !    3    (Lines #1 .. #6 in this function and in the Vapp function
!		else                           !    4     must be adjusted for each kind of potential wave shape  
!			V01=V0*4.0/pi/mf           !    5     according to its Fourier Transformation)
!		end if						   !    6
	
	! ...... SAWTOOTH WAVE ...................................................................................
		Omega=2.0*mf*pi/Tl             !    1	 Frequency of Wave in Fourier Series	 
		if (mf/=0)	V01=-V0/pi/mf				   
		
	! ........................................................................................................                       
		
		if (mf==0) then
			V1=Cdn/(Cdn+Can)*Vapp			! For DC Part of Applied Voltage
		else
			Vapp1=V01*sin(omega*Time1)		! Applied Voltage in the Specific Frequancy
			Vapp=Vapp+Vapp1
           
			Rn=rua*Ln/(zn*hn)			!  Resistance of nth Sub-Circut
			if(Vapp1<0.0)    Rn=5.0*Rn
			Z1=Rn						!  Impedance of Resistance		
			Z2=1.0/(Can*omega*j)		!  Impedance of Air Capacitor
			Z3=1.0/(Cdn*omega*j)		!  Impedance of Dielectric Capacitor

			Z=Z3+Z2
			C=Z3/Z
			Phiv=atan2(aimag(C),real(C))
			V1=V01*abs(C)*sin(omega*Time1+Phiv)

			if((abs(Vapp1-V1)>Vi)) then 	
				Z12=1.0/((1.0/Z1)+(1.0/Z2))
				Z=Z3+Z12
				C=Z3/Z
				Phiv=atan2(aimag(C),real(C))
				V1=V01*abs(C)*sin(omega*Time1+Phiv)
				I1=(Vapp1-V1)/Z12			! CALCULATION OF CURRENT THROUGH PLASMA
				Ieff(m,k)=Ieff(m,k)+I1
                  Pt(k)=Pt(k)+(Vapp1-V1)**2.0/Rn	! Dissipated Power
			end if
		end if
		
          LMBC=LMBC+V1
	    mf=mf+1
		if(mf==NF) then
			exit Fourier_L
		end if
	end do Fourier_L

! ... FIND BOUNDARY OF IONIZED AND NON-IONIZED REGIONS .......................................................

	If((abs(Vapp-LMBC)>Vi)) then 
		NCI=m
	end if
! ............................................................................................................

	Iptot(k)=Iptot(k)+Ieff(m,k)

    Return
    End Function LMBC
