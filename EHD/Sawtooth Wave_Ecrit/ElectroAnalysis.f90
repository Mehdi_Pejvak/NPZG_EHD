


include 'Geom.fi'
include 'ProbCons.fi'

Program Potential_1

!__ Main Program: _________________________________________________________________

use Geom
use ProbCons
use portlib

implicit none

integer:: i,m,k,tt
real:: deltaT,Time1,t2,Opt_Time
Real (Kind=8) :: Ptot,It

Opt_Time=timef()

deltaT=(1./f)/NT
Tl=1/f
Ktotal=1*NT+1

Call GeomInput
Call Geom_Char
print*, ">>> Number of Time Step= ", ktotal-1
print*, " ... Reading Geometry Inputs Were Carried Out ..."

er=1.0
k=0
Time1=0.0
! ---- INITIAL CONDITIONS ----------------------------------------------------------
pv=0.0
pt=0.0
ieff=0.0
Iptot=0.0
It=0.0
Ptot=0.0
p(1:E)=1.0
! ----------------------------------------------------------------------------------
open(70,file="C:\MY FOLDER\Project\Code\CFD\Flat Plate\EHD2CFD\EHD\Sawtooth Wave_Ecrit\Data\Report_Resultd-2.txt")

Time_L: do k=1,ktotal
	NCI=0
	
	print*,"---------------------------------------------------------------------------"
    Write(70,*) "---------------------------------------------------------------------------"
	Time1=deltaT*real(k-1)
	Vout(k)=Vapp(Time1)		! Value of Potential on the Surface of Exposed Electrode

	LMBC_L: do m=1,m13
		Pv(m+(m10+m11+m12))=LMBC(m,Time1,deltaT,k)		! Value of Potential On the Surface of Virtual Electrode
	end do LMBC_L

	print*," ...  LECM-based Potentials Were Calculated For the New Time Step."
    Print "(A48,I5)",">>> No. of Cell Located on the Ionized Region: ",NCI
    Write(70,*)" No. of Cell Located on the Ionized Region: ",NCI
	i=0

!	p(1:E)=Vout	! Initial Condition For First Time Step of Potential Equ.
	Ptot=Ptot+Pt(k)	! Total Dissipated Power Over a Period
	It=It+Iptot(k)	
	er=1.0
	Main_L: do
		i=i+1
		CALL PotenIter (i,k)

		if((mod(I,IOR)==0 .or. er<=Tol_abs))		then
            print "(A10,I6,A13,I6,A15,E15.7)","Time Step:",k-1,", Iteration:",i,", Max Error:",er
            Write(70, "(A10,I6,A13,I6,A15,E15.7)") ,"Time Step:",k-1,", Iteration:",i,", Max Error:",er
        end if
		if(er<=Tol_abs .or. i>1000) then
			print*," ... Convergence! See the results for the current time step."

			do tt=1,N
				P_nodal(tt,k)=NodalP(tt,k)	 
			end do
				
			exit Main_L
		end if

	end do Main_L

	Call Body_Force (k)
!	if (mod(k,10)==0) Call Output
	
end do Time_L

print '(/,A76)'," ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"
call Output        
print*,">>> Calculations for all of the time steps are completed. See the final result."

Opt_Time=timef()
Print*, " Time of Operation= " , Opt_Time
Print*, " Dissipated Power=",Ptot
Print*, " Total Displaced Charges=",It*deltaT

Write(70,*) " Time of Operation= " , Opt_Time
Write(70,*) " Dissipated Power=",Ptot
Write(70,*) " Total Displaced Charges=",It*deltaT
close(70)

Pause

Contains

!.. Subroutines: ..................................................................

include 'Geometry_Input.fi'
include 'Geom_Char.fi'
include 'Iteration.fi'
include 'Output.fi'
include 'ExpTerm.fi'
include 'Body_Force.fi'
include 'GMRES_m.fi'
include 'mult_givens.fi'
include 'GS.fi'

!.. Functions: ....................................................................


include 'Nodal_Poten.fi'
include 'LMBC.fi'
include 'Vapp_Square.fi'

!__________________________________________________________________________________

End Program Potential_1
