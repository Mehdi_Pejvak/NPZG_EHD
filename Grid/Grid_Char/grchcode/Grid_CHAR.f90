

!include "NEnumber.f90"


Program GRID_CHAR

!__ Main Program: _________________________________________________________________

Use GCNEnumber
!Use portlib

Implicit None


Real :: op_time


!	op_time=timef()

! Caution: If surface is an airfoil change Xa,Ya in "normal_dis" subroutine   

! --- READING GRID DATA ---------------------------
	 Print*, ">>> Reading Input Data From Files ..."
	 Call Input

! --- REQUIREMENTS ACHEIVEMENT ---------------------------
	 Print*, ">>> Calculation of Requirement is Underway ..."

	 print*,' > Part 1: Obtaining Grid Chrarcter (Adjacent Cell Of Each Node And Cell)' 

	 Call USg_CHAR	! REQUERED POINT FOR GRID CONSIST OF SHARED POINT AND NEIGHBOURED RECTANGULAR

	 print*,' > Part 2: Obtaining All Requirements For Applying QUICK Method'

	 Call QUICK		! OBTAINING POINT FOR APPLYING QUICK METHOD IN UNSTRUCTURED GRID

!	 print*,' > Part 3: Obtaining All Boundary Control Volumes' 

!	 Call boundary	! OBTAINING POINT FOR APPLYING NIEUMANN BOUNDARY CONDITION

	 print*,' > Part 3: Obtaining Normal Distance From the Nearest Wall' 

	 Call Normal_dis	! OBTAINING POINT FOR APPLYING TURBULRNT AND TRANSITION MODEL

	 print*,' > Part 4: Obtaining the Parameters Necessary For Applying EHD Force' 

	 Call EHD_CHAR	 

! --- WRITING OBTAINED DATA ---------------------------------
	 Print*, ">>> Writing Data on Files ..."
	 	 
	 Call Output

!	 op_time=timef()
	 print*,'TIME OF PROCESS=' , OP_TIME

!	 pause

Contains

!__ Subroutines _______________________________________________________________________

include "USg_CHAR.f90"
include "QUICK.f90"
include "EHD_CHAR.f90"
include "Normal_dis.f90"
include "Output.f90"
include "Input.f90"

!______________________________________________________________________________________

End Program GRID_CHAR
