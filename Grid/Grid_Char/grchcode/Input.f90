
 Subroutine Input


 use GCNEnumber

 implicit none

 integer:: i,t

 Real(Kind = 8) ::	Kp,LP,MP

 Open(10,file='C:\MY FOLDER\Project\Code\CFD\Flat Plate\EHD2CFD\Grid\Grid Character\ZFP_GG_ECFDt2.plt')
	Open(100,file='C:\MY FOLDER\Project\Code\CFD\Flat Plate\EHD2CFD\Grid\Grid Character\ZFP_GC_ECFDt2.dat')

11 Format(/,8X,I13,5X,I13)
    read(10,11) N,E
    print*,">> N=",N," , E=",E

	allocate(x(N))
	allocate(y(N))

	allocate(Xp(E))
	allocate(Yp(E))

	allocate(Con(E,4))
    allocate(Neibr(E,4))
	allocate(Cn(E))
	allocate(FP(E,4))

    
16 Format(32X,2I6)
    read(10,16) n1,n2
    print '(A31,I4,A20,I4)'," Number of Cell in X Dirextion:",n1,"  ,  in Y Direction:",n2

	n3 = n1
	n4 = n2
	mb = n1 + n2 + n3 + n4
	m = n1 + n2 + n3 + n4

161 Format(34X,2E19.8)
	 read(10,161) H,Lf
	 print '(A8,E19.8,A11,E19.8)'," Height=",H," ,  Length=",Lf

12  Format(2E24.5)
	 do i = 1,N
	    read(10,12) x(i),y(i)
	 end do

13  Format(4I7)
	 do i = 1,E
	    read(10,13) Con(i,1:4)
	 end do

	 print*," x(1) :",x(1)," , y(1) : ",y(1)
	 print*," x(N) :",x(N)," , y(N) : ",y(N)
	 print*," Con(1,1) :",Con(1,1)," , Con(1,2) : ",Con(1,2)," , Con(1,3) : ",Con(1,3)," , Con(1,4) : ",Con(1,4)
	 print*," Con(E,1) :",Con(E,1)," , Con(E,2) : ",Con(E,2)," , Con(E,3) : ",Con(E,3)," , Con(E,4) : ",Con(E,4)
	 close (10)

23  Format(I6)
24  Format(I6,3E15.7)

	 do i = 1,E
		read(100,23) cn(i)
		do t = 1,4
			read (100,24) FP(i,t),KP,LP,MP 
		end do
	 end do

	 print*,"Location of Faces of Cell No. 1 :",fp(1,1:4)
	 print*,"Location of Faces of Cell No. E :",fp(E,1:4)

	 pause

	 Rewind(100)

 Return

 End  Subroutine Input

