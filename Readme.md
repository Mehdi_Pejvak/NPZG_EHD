The code developed in Intel FORTRAN programming language to solve numerically a fluid flow over an airfoil exposed to adverse pressure gradient. The flow regime in transition between Laminar flow to turbulence, therefore the flow speed is not high enough to overcome this adverse pressure gradient and therefore experience separation. Then by using and electrohydrodynimcs actuator and imposing force on idolized fluid we can solve the problem of the separation.
Navier-Stokes equation descritised by Finite volume method and to solve the system of equation, Gauss-Seidel iterative method is used.

The library/packages used in the code is

#### 1- [Intel FORTRAN](https://www.intel.com/content/www/us/en/developer/tools/oneapi/fortran-compiler.html)